package BusinessLogic;

import BusinessLogic.Validators.OrdersAmountPricePaidValidator;
import BusinessLogic.Validators.Validator;
import DataAccess.OrdersDAO;
import Model.Orders;

import java.util.ArrayList;
import java.util.List;

public class OrdersBl
{
    private List<Validator<Orders>> validators;
    private OrdersDAO ordersDAO;

    public OrdersBl()
    {
        ordersDAO = new OrdersDAO();
        validators = new ArrayList<>();
        validators.add(new OrdersAmountPricePaidValidator());
    }

    public void insert(Orders o)
    {
        for (Validator<Orders> v : validators)
        {
            v.validate(o);
        }

        ordersDAO.insert(o);
    }
}
