package BusinessLogic;

import BusinessLogic.Validators.ProductPriceWeightValidator;
import BusinessLogic.Validators.Validator;
import DataAccess.ProductDAO;
import Model.Product;

import java.util.List;
import java.util.NoSuchElementException;

public class ProductBl
{
    private Validator<Product> validator;
    private ProductDAO productDAO;

    public ProductBl()
    {
        productDAO = new ProductDAO();
        validator = new ProductPriceWeightValidator();
    }

    public Product findById(int id)
    {
        Product p = null;

        p = productDAO.findById(id);

        if(p == null)
        {
            throw new NoSuchElementException("Product id " + id + "does not exist");
        }

        return p;
    }

    public List<Product> findAll()
    {
        return productDAO.findAll();
    }

    public void insert(Product p)
    {
        validator.validate(p);

        productDAO.insert(p);
    }

    public void delete(int id)
    {
        findById(id);

        productDAO.delete(id);
    }

    public void update(int id, Product p)
    {
        findById(id);

        validator.validate(p);

        productDAO.update(id, p);
    }
}
