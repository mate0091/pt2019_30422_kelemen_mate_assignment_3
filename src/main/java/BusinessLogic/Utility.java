package BusinessLogic;

import Model.Client;
import Model.Orders;
import Model.Product;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;

public class Utility
{
    public static <T> DefaultTableModel createModel(List<T> obj)
    {
        List<String> columns = new ArrayList<>();
        DefaultTableModel model;

        if(obj.size() > 0)
        {
            for (Field f : obj.get(0).getClass().getDeclaredFields())
            {
                columns.add(f.getName());
            }
        }

        model = new DefaultTableModel(columns.toArray(), 0);

        //add rows with reflection
        for(T o : obj)
        {
            Vector<String> row = new Vector<>();

            for(Field f : o.getClass().getDeclaredFields())
            {
                f.setAccessible(true);

                try {
                    String value = f.get(o).toString();
                    row.add(value);
                }

                catch (IllegalAccessException e)
                {
                    e.printStackTrace();
                }
            }

            model.addRow(row);
        }

        return model;
    }

    public static void createReceipt(Orders order, Product product, Client client)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd_HH_mm_ss");
        String fileLocation = "./" + client.getFname() + "_" + client.getLname() + "_" + product.getName() + "_" + sdf.format(Calendar.getInstance().getTime()) + ".pdf";

        Document doc = new Document();

        try
        {
            PdfWriter.getInstance(doc, new FileOutputStream(new File(fileLocation)));

            //open
            doc.open();

            Font font1 = new Font(Font.FontFamily.TIMES_ROMAN, 24, Font.BOLD);
            Font font2 = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
            Font font3 = new Font(Font.FontFamily.TIMES_ROMAN, 12);

            Paragraph titlep = new Paragraph();
            titlep.setAlignment(Element.ALIGN_CENTER);
            titlep.setSpacingAfter(24.0f);
            titlep.setFont(font1);
            titlep.add("Order details");

            Paragraph productP = new Paragraph();
            productP.setFont(font2);
            productP.setSpacingAfter(15.0f);
            productP.setAlignment(Element.ALIGN_LEFT);
            productP.add("Product:");

            Paragraph productInfoP = new Paragraph();
            productInfoP.setFont(font3);
            productInfoP.setSpacingAfter(15.0f);
            productInfoP.setAlignment(Element.ALIGN_LEFT);
            productInfoP.add(product.toString());

            Paragraph clientP = new Paragraph();
            clientP.setFont(font2);
            clientP.setSpacingAfter(15.0f);
            clientP.setAlignment(Element.ALIGN_LEFT);
            clientP.add("Client:");

            Paragraph clientInfoP = new Paragraph();
            clientInfoP.setFont(font3);
            clientInfoP.setSpacingAfter(15.0f);
            clientInfoP.setAlignment(Element.ALIGN_LEFT);
            clientInfoP.add(client.toString());

            Paragraph orderP = new Paragraph();
            orderP.setFont(font2);
            orderP.setSpacingAfter(15.0f);
            orderP.setAlignment(Element.ALIGN_LEFT);
            orderP.add("Details: ");

            Paragraph orderInfoP = new Paragraph();
            orderInfoP.setFont(font3);
            orderInfoP.setSpacingAfter(15.0f);
            orderInfoP.setAlignment(Element.ALIGN_LEFT);
            orderInfoP.add(order.toString());

            doc.add(titlep);
            doc.add(productP);
            doc.add(productInfoP);
            doc.add(clientP);
            doc.add(clientInfoP);
            doc.add(orderP);
            doc.add(orderInfoP);

            doc.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
