package BusinessLogic;

import BusinessLogic.Validators.ClientAgeValidator;
import BusinessLogic.Validators.ClientEmailValidator;
import BusinessLogic.Validators.ClientNameValidator;
import BusinessLogic.Validators.Validator;
import DataAccess.ClientDAO;
import Model.Client;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ClientBl
{
    private List<Validator<Client>> validators;
    private ClientDAO clientDAO;

    public ClientBl()
    {
        this.clientDAO = new ClientDAO();
        this.validators = new ArrayList<>();
        this.validators.add(new ClientEmailValidator());
        this.validators.add(new ClientNameValidator());
        this.validators.add(new ClientAgeValidator());
    }

    public Client findById(int id)
    {
        Client c = null;

        c = clientDAO.findById(id);

        if(c == null)
        {
            throw new NoSuchElementException("Client id " + id + "Does not exist");
        }

        return c;
    }

    public List<Client> findAll()
    {
        return clientDAO.findAll();
    }

    public void insert(Client c)
    {
        for(Validator<Client> v : validators)
        {
            v.validate(c);
        }

        clientDAO.insert(c);
    }

    public void delete(int id)
    {
        findById(id);

        clientDAO.delete(id);
    }

    public void update(int id, Client c)
    {
        findById(id);

        for(Validator<Client> v : validators)
        {
            v.validate(c);
        }

        clientDAO.update(id, c);
    }
}
