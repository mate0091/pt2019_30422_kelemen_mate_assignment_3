package BusinessLogic.Validators;

import Model.Orders;

public class OrdersAmountPricePaidValidator implements Validator<Orders>
{
    @Override
    public void validate(Orders orders)
    {
        if(orders.getAmount() <= 0 || orders.getPrice_paid() <= 0) throw new IllegalArgumentException("Amount and price paid have to be greater than 0");
    }
}
