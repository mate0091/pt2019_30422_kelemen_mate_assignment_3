package BusinessLogic.Validators;

import Model.Client;

public class ClientNameValidator implements Validator<Client>
{
    @Override
    public void validate(Client client)
    {
        String fname = client.getFname();
        String lname = client.getLname();

        validateName(fname);
        validateName(lname);
    }

    private void validateName(String name)
    {
        char[] chars = name.toCharArray();

        if(!Character.isUpperCase(chars[0])) throw new IllegalArgumentException("Invalid name");

        for (int i = 1; i < chars.length; i++)
        {
            if(!Character.isLetter(chars[i]) || !Character.isLowerCase(chars[i])) throw new IllegalArgumentException("Invalid name");
        }
    }
}
