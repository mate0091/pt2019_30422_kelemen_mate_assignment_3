package BusinessLogic.Validators;

import Model.Client;

public class ClientAgeValidator implements Validator<Client>
{
    @Override
    public void validate(Client client)
    {
        if(client.getAge() < 14) throw new IllegalArgumentException("Client too young");
    }
}
