package BusinessLogic.Validators;

import Model.Client;

public class ClientEmailValidator implements Validator<Client>
{
    @Override
    public void validate(Client client)
    {
        String email = client.getEmail();

        if(!email.contains(".") || !email.contains("@")) throw new IllegalArgumentException("Invalid email");
    }
}
