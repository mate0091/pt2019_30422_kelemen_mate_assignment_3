package BusinessLogic.Validators;

import Model.Product;

public class ProductPriceWeightValidator implements Validator<Product>
{
    @Override
    public void validate(Product product)
    {
        if(product.getPrice() <= 0.0f || product.getWeight() <= 0.0f) throw new IllegalArgumentException("Price and weight has to be greater than 0");
    }
}
