package Main;

import DataAccess.AbstractDAO;
import DataAccess.ClientDAO;
import DataAccess.ProductDAO;
import Model.Client;
import Model.Product;
import MysqlConnection.ConnectionFactory;
import Presentation.Controller;
import Presentation.Model;
import Presentation.View;

import java.util.List;

public class Main
{
    private View view;
    private Controller controller;
    private Model model;

    //private ClientDAO clientDAO;

    public Main()
    {
        this.model = new Model();
        this.view = new View(model);
        this.controller = new Controller(this.view, this.model);

        //clientDAO = new ClientDAO();

        //Client c = new Client(11, "Kelemen", "Gyork", 15, "gyork.k@gmail.com", "Baita 8");

        //clientDAO.update(11, c);
    }

    public static void main(String[] args)
    {
        new Main();
    }
}
