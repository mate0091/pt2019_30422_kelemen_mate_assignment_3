package DataAccess;

import Model.Product;

import java.sql.ResultSet;
import java.util.List;

public class ProductDAO extends AbstractDAO<Product>
{
    @Override
    public Product findById(int id) {
        return super.findById(id);
    }

    @Override
    public List<Product> findAll() {
        return super.findAll();
    }

    @Override
    public void insert(Product object) {
        super.insert(object);
    }

    @Override
    public void delete(int id) {
        super.delete(id);
    }

    @Override
    public void update(int id, Product object) {
        super.update(id, object);
    }

    @Override
    public List<Product> createObjects(ResultSet rs) {
        return super.createObjects(rs);
    }
}
