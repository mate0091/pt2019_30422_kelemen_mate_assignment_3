package DataAccess;

import Model.Orders;
import MysqlConnection.ConnectionFactory;

import java.sql.*;
import java.util.List;

public class OrdersDAO extends AbstractDAO<Orders>
{
    @Override
    public Orders findById(int id) {
        return super.findById(id);
    }

    @Override
    public List<Orders> findAll() {
        return super.findAll();
    }

    @Override
    public void insert(Orders object) {
        super.insert(object);
    }

    @Override
    public void delete(int id) {
        super.delete(id);
    }

    @Override
    public void update(int id, Orders object) {
        super.update(id, object);
    }

    @Override
    public List<Orders> createObjects(ResultSet rs) {
        return super.createObjects(rs);
    }
}
