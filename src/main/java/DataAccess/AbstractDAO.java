package DataAccess;

import Model.Client;
import MysqlConnection.ConnectionFactory;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AbstractDAO<T>
{
    private final Class<T> type;

    public AbstractDAO()
    {
        type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    private String getFields(boolean questionMarks)
    {
        Field[] fields = type.getDeclaredFields();
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < fields.length; i++)
        {
            if(i != 0)
            {
                if(questionMarks) sb.append("?");
                else sb.append(fields[i].getName());

                if(i != fields.length - 1)
                {
                    sb.append(", ");
                }
            }
        }

        return sb.toString();
    }

    private String updatePairs()
    {
        Field[] fields = type.getDeclaredFields();
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < fields.length; i++)
        {
            if(i != 0)
            {
                sb.append(fields[i].getName() + "=?");

                if(i != fields.length - 1)
                {
                    sb.append(", ");
                }
            }
        }

        return sb.toString();
    }

    public T findById(int id)
    {
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        String query = "SELECT * FROM orders." + type.getSimpleName() + " WHERE id=" + id;

        try
        {
            conn = ConnectionFactory.getConnection();
            statement = conn.prepareStatement(query);

            rs = statement.executeQuery();

            return createObjects(rs).get(0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        finally
        {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(conn);
        }

        return null;
    }

    public List<T> findAll()
    {
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        String query = "SELECT * FROM orders." + type.getSimpleName();

        try
        {
            conn = ConnectionFactory.getConnection();
            statement = conn.prepareStatement(query);

            rs = statement.executeQuery();

            return createObjects(rs);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        finally
        {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(conn);
        }

        return null;
    }

    public void insert(T object)
    {
        Connection conn = null;
        PreparedStatement statement = null;

        String query = "INSERT INTO orders." + type.getSimpleName() + " (" + this.getFields(false) + ") VALUES (" + this.getFields(true) + ")";

        try
        {
            conn = ConnectionFactory.getConnection();
            statement = conn.prepareStatement(query);

            Field[] fields = type.getDeclaredFields();

            for (int i = 1; i < fields.length; i++)
            {
                fields[i].setAccessible(true);
                //object values
                Object value = fields[i].get(object);
                statement.setObject(i, value);
            }

            statement.executeUpdate();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        finally
        {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(conn);
        }
    }

    public void delete(int id)
    {
        Connection dbc = ConnectionFactory.getConnection();
        PreparedStatement statement = null;

        String query = "delete from orders." + type.getSimpleName() + " where id=?";

        try{
            statement = dbc.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(dbc);
        }
    }

    public void update(int id, T object)
    {
        Connection conn = null;
        PreparedStatement statement = null;

        String query = "UPDATE orders." + type.getSimpleName() + " SET " + this.updatePairs() + " WHERE id=" + id;

        try
        {
            conn = ConnectionFactory.getConnection();
            statement = conn.prepareStatement(query);

            Field[] fields = type.getDeclaredFields();

            for (int i = 1; i < fields.length; i++)
            {
                fields[i].setAccessible(true);
                //object values
                Object value = fields[i].get(object);
                statement.setObject(i, value);
            }

            statement.executeUpdate();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        finally
        {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(conn);
        }
    }

    public List<T> createObjects(ResultSet rs)
    {
        List<T> results = new ArrayList<>();

        try
        {
            while (rs.next())
            {
                //create empty
                T instance = type.newInstance();

                for (Field field : type.getDeclaredFields())
                {
                    field.setAccessible(true);
                    Object value = rs.getObject(field.getName());
                    field.set(instance, value);
                }
                results.add(instance);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return results;
    }
}
