package DataAccess;

import Model.Client;
import MysqlConnection.ConnectionFactory;

import java.sql.*;
import java.util.*;

public class ClientDAO extends AbstractDAO<Client>
{
    @Override
    public Client findById(int id) {
        return super.findById(id);
    }

    @Override
    public List<Client> findAll() {
        return super.findAll();
    }

    @Override
    public void delete(int id) {
        super.delete(id);
    }

    @Override
    public List<Client> createObjects(ResultSet rs) {
        return super.createObjects(rs);
    }

    @Override
    public void insert(Client object) {
        super.insert(object);
    }

    @Override
    public void update(int id, Client object) {
        super.update(id, object);
    }
}
