package Presentation;

import Model.*;

public class Controller
{
    private View view;
    private Model model;

    public Controller(View view, Model model)
    {
        this.view = view;
        this.model = model;

        this.view.addClientSearchListener(e ->
        {
            Client c = this.model.searchClient(this.view.getId("Client"));
            this.view.setClientText(c);
        });

        this.view.addProductSearchListener(e ->
        {
            Product p = this.model.searchProduct(this.view.getId("Product"));
            this.view.setProductText(p);
        });

        this.view.addClientInsertListener(e ->
        {
            this.model.insertClient(this.view.enteredClient());
            this.model.refreshTable("Client");
            this.view.refreshTable("Client", this.model.getClientModel());
        });

        this.view.addClientDeleteListener(e ->
        {
            this.model.deleteClient(this.view.getId("Client"));
            this.model.refreshTable("Client");
            this.view.refreshTable("Client", this.model.getClientModel());
        });

        this.view.addClientUpdateListener(e ->
        {
            this.model.updateClient(this.view.enteredClient());
            this.model.refreshTable("Client");
            this.view.refreshTable("Client", this.model.getClientModel());
        });

        this.view.addProductInsertListener(e ->
        {
            this.model.insertProduct(this.view.enteredProduct());
            this.model.refreshTable("Product");
            this.view.refreshTable("Product", this.model.getProductModel());
        });

        this.view.addProductDeleteListener(e ->
        {
            this.model.deleteProduct(this.view.getId("Product"));
            this.model.refreshTable("Product");
            this.view.refreshTable("Product", this.model.getProductModel());
        });

        this.view.addProductUpdateListener(e ->
        {
            this.model.updateProduct(this.view.enteredProduct());
            this.model.refreshTable("Product");
            this.view.refreshTable("Product", this.model.getProductModel());
        });

        this.view.addPlaceOrderListener(e -> {
            try
            {
                this.model.placeOrder(this.view.enteredOrders());
                this.model.refreshTable("Product");
                this.view.refreshTable("Product", this.model.getProductModel());
            }
            catch (Exception e1)
            {
                e1.printStackTrace();
            }
        });
    }
}
