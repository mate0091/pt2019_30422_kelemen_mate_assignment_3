package Presentation;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import BusinessLogic.ClientBl;
import BusinessLogic.OrdersBl;
import BusinessLogic.ProductBl;
import BusinessLogic.Utility;
import Model.*;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class Model
{
    private DefaultTableModel clientModel;
    private DefaultTableModel productModel;
    private ClientBl clientBl;
    private ProductBl productBl;
    private OrdersBl ordersBl;

    public Model()
    {
        this.clientBl = new ClientBl();
        this.productBl = new ProductBl();
        this.ordersBl = new OrdersBl();

        this.refreshTable("Client");
        this.refreshTable("Product");
    }

    public void refreshTable(String option)
    {
        if(option.compareToIgnoreCase("Client") == 0)
        {
            List<Client> clients = clientBl.findAll();
            clientModel = Utility.createModel(clients);
        }

        else
        {
            List<Product> products = productBl.findAll();
            productModel = Utility.createModel(products);
        }
    }

    public DefaultTableModel getClientModel() {
        return clientModel;
    }

    public DefaultTableModel getProductModel() {
        return productModel;
    }

    public Client searchClient(int id)
    {
        return clientBl.findById(id);
    }

    public Product searchProduct(int id)
    {
        return productBl.findById(id);
    }

    public void insertClient(Client client)
    {
        clientBl.insert(client);
    }

    public void insertProduct(Product product)
    {
        productBl.insert(product);
    }

    public void deleteClient(int id)
    {
        clientBl.delete(id);
    }

    public void deleteProduct(int id)
    {
        productBl.delete(id);
    }

    public void updateClient(Client client)
    {
        clientBl.update(client.getId(), client);
    }

    public void updateProduct(Product product)
    {
        productBl.update(product.getId(), product);
    }

    public void placeOrder(Orders order)
    {
        Client client = clientBl.findById(order.getClientid());
        Product product = productBl.findById(order.getProductid());

        if(product.getStock() - order.getAmount() < 0)
        {
            JOptionPane.showMessageDialog(null, "Product " + product.getName() + " is understocked.", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }

        Product decrementedProduct = new Product(product.getId(), product.getName(), product.getType(), product.getPrice(), product.getDescription(), product.getWeight(), product.getStock() - order.getAmount());

        productBl.update(product.getId(), decrementedProduct);
        ordersBl.insert(order);

        //create order file here
        Utility.createReceipt(order, product, client);
    }
}
