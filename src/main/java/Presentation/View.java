package Presentation;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.sql.Date;

import Model.*;

public class View extends JFrame
{
    private Model model;

    private JTable clientTable;
    private JTable productTable;

    private JButton searchClientBtn;
    private JButton insertClientBtn;
    private JButton deleteClientBtn;
    private JButton updateClientBtn;

    private JButton searchProductBtn;
    private JButton insertProductBtn;
    private JButton deleteProductBtn;
    private JButton updateProductBtn;

    private JButton placeOrderBtn;

    private JTextField clientFName;
    private JTextField clientLName;
    private JTextField clientId;
    private JTextField clientAge;
    private JTextField clientEmail;
    private JTextField clientAddress;

    private JTextField productId;
    private JTextField productName;
    private JTextField productType;
    private JTextField productPrice;
    private JTextField productDesc;
    private JTextField productWeight;
    private JTextField productStock;

    private JTextField orderProductId;
    private JTextField orderClientId;
    private JTextField orderAmount;
    private JTextField orderPricePaid;
    private JTextField orderAddress;

    private JPanel clientPanel;
    private JPanel productPanel;
    private JPanel orderPanel;

    private JPanel clientEditPanel;
    private JPanel productEditPanel;

    private JTabbedPane tabs;

    public View(Model model)
    {
        this.model = model;

        this.searchClientBtn = new JButton("Search");
        this.insertClientBtn = new JButton("Insert");
        this.deleteClientBtn = new JButton("Delete");
        this.updateClientBtn = new JButton("Update");

        this.searchProductBtn = new JButton("Search");
        this.insertProductBtn = new JButton("Insert");
        this.deleteProductBtn = new JButton("Delete");
        this.updateProductBtn = new JButton("Update");

        this.placeOrderBtn = new JButton("Place Order");

        this.model.refreshTable("Client");
        this.model.refreshTable("Product");

        this.clientTable = new JTable(this.model.getClientModel());
        this.productTable = new JTable(this.model.getProductModel());

        //components
        clientPanel = new JPanel(new GridLayout(1, 2));
        clientEditPanel = new JPanel(new FlowLayout());
        clientPanel.add(new JScrollPane(this.clientTable));

        //client edit
        clientFName = new JTextField(20);
        clientId = new JTextField(20);
        clientLName = new JTextField(20);
        clientAge = new JTextField(20);
        clientEmail = new JTextField(20);
        clientAddress = new JTextField(20);

        clientEditPanel.add(new JLabel("id:        "));
        clientEditPanel.add(clientId);
        clientEditPanel.add(new JLabel("first name:"));
        clientEditPanel.add(clientFName);
        clientEditPanel.add(new JLabel("last name: "));
        clientEditPanel.add(clientLName);
        clientEditPanel.add(new JLabel("age:       "));
        clientEditPanel.add(clientAge);
        clientEditPanel.add(new JLabel("email:     "));
        clientEditPanel.add(clientEmail);
        clientEditPanel.add(new JLabel("address:   "));
        clientEditPanel.add(clientAddress);

        clientEditPanel.add(searchClientBtn);
        clientEditPanel.add(insertClientBtn);
        clientEditPanel.add(deleteClientBtn);
        clientEditPanel.add(updateClientBtn);
        clientPanel.add(clientEditPanel);

        productPanel = new JPanel(new GridLayout(1, 2));
        productEditPanel = new JPanel(new FlowLayout());
        productPanel.add(new JScrollPane(this.productTable));

        //product edit
        productId = new JTextField(20);
        productName = new JTextField(20);
        productType = new JTextField(20);
        productPrice = new JTextField(20);
        productDesc = new JTextField(20);
        productWeight = new JTextField(20);
        productStock = new JTextField(20);

        productEditPanel.add(new JLabel("id:         "));
        productEditPanel.add(productId);
        productEditPanel.add(new JLabel("name:       "));
        productEditPanel.add(productName);
        productEditPanel.add(new JLabel("type:       "));
        productEditPanel.add(productType);
        productEditPanel.add(new JLabel("price:      "));
        productEditPanel.add(productPrice);
        productEditPanel.add(new JLabel("description:"));
        productEditPanel.add(productDesc);
        productEditPanel.add(new JLabel("weight:     "));
        productEditPanel.add(productWeight);
        productEditPanel.add(new JLabel("stock:      "));
        productEditPanel.add(productStock);

        productEditPanel.add(searchProductBtn);
        productEditPanel.add(insertProductBtn);
        productEditPanel.add(deleteProductBtn);
        productEditPanel.add(updateProductBtn);
        productPanel.add(productEditPanel);

        //orders
        orderClientId = new JTextField(10);
        orderProductId = new JTextField(10);
        orderPricePaid = new JTextField(10);
        orderAddress = new JTextField(10);
        orderAmount = new JTextField(10);

        orderPanel = new JPanel(new GridLayout(6, 1));

        Container clientId = new Container();
        Container productId = new Container();
        Container pricePaidId = new Container();
        Container address = new Container();
        Container amount = new Container();

        clientId.setLayout(new FlowLayout());
        productId.setLayout(new FlowLayout());
        pricePaidId.setLayout(new FlowLayout());
        address.setLayout(new FlowLayout());
        amount.setLayout(new FlowLayout());

        clientId.add(new JLabel("Client id:"));
        clientId.add(orderClientId);

        productId.add(new JLabel("Product id:"));
        productId.add(orderProductId);

        pricePaidId.add(new JLabel("Price paid:"));
        pricePaidId.add(orderPricePaid);

        address.add(new JLabel("Address:"));
        address.add(orderAddress);

        amount.add(new JLabel("Amount:"));
        amount.add(orderAmount);

        orderPanel.add(clientId);
        orderPanel.add(productId);
        orderPanel.add(pricePaidId);
        orderPanel.add(address);
        orderPanel.add(amount);
        orderPanel.add(placeOrderBtn);

        this.tabs = new JTabbedPane();
        this.tabs.addTab("Clients", clientPanel);
        this.tabs.addTab("Products", productPanel);
        this.tabs.addTab("Orders", orderPanel);

        this.add(tabs);
        this.setSize(650, 400);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setResizable(false);
        this.setTitle("Order Management");
    }

    public void refreshTable(String option, DefaultTableModel model)
    {
        if(option.compareToIgnoreCase("Client") == 0)
        {
            clientTable.setModel(model);
        }

        else
        {
            productTable.setModel(model);
        }
    }

    public void addClientSearchListener(ActionListener e){this.searchClientBtn.addActionListener(e);}

    public void addClientInsertListener(ActionListener e)
    {
        this.insertClientBtn.addActionListener(e);
    }

    public void addClientDeleteListener(ActionListener e)
    {
        this.deleteClientBtn.addActionListener(e);
    }

    public void addClientUpdateListener(ActionListener e)
    {
        this.updateClientBtn.addActionListener(e);
    }

    public void addProductSearchListener(ActionListener e)
    {
        this.searchProductBtn.addActionListener(e);
    }

    public void addProductInsertListener(ActionListener e)
    {
        this.insertProductBtn.addActionListener(e);
    }

    public void addProductDeleteListener(ActionListener e)
    {
        this.deleteProductBtn.addActionListener(e);
    }

    public void addProductUpdateListener(ActionListener e)
    {
        this.updateProductBtn.addActionListener(e);
    }

    public void addPlaceOrderListener(ActionListener e)
    {
        this.placeOrderBtn.addActionListener(e);
    }

    public Client enteredClient()
    {
        return new Client(Integer.parseInt(clientId.getText()), clientFName.getText(), clientLName.getText(), Integer.parseInt(clientAge.getText()), clientEmail.getText(), clientAddress.getText());
    }

    public Product enteredProduct()
    {
        return new Product(Integer.parseInt(productId.getText()), productName.getText(), productType.getText(), Float.parseFloat(productPrice.getText()), productDesc.getText(), Float.parseFloat(productWeight.getText()), Integer.parseInt(productStock.getText()));
    }

    public Orders enteredOrders()
    {
        return new Orders(Integer.parseInt(orderClientId.getText()), Integer.parseInt(orderProductId.getText()), Integer.parseInt(orderAmount.getText()), Float.parseFloat(orderPricePaid.getText()), new Date(System.currentTimeMillis()), orderAddress.getText());
    }

    public void setClientText(Client client)
    {
        clientFName.setText(client.getFname());
        clientLName.setText(client.getLname());
        clientAge.setText(Integer.toString(client.getAge()));
        clientEmail.setText(client.getEmail());
        clientAddress.setText(client.getAddress());
    }

    public void setProductText(Product product)
    {
        productName.setText(product.getName());
        productDesc.setText(product.getDescription());
        productType.setText(product.getType());
        productPrice.setText(Float.toString(product.getPrice()));
        productWeight.setText(Float.toString(product.getWeight()));
        productStock.setText(Integer.toString(product.getStock()));
    }

    public int getId(String option)
    {
        if(option == "Client")
        {
            return Integer.parseInt(clientId.getText());
        }

        else return Integer.parseInt(productId.getText());
    }
}
