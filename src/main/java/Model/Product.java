package Model;

public class Product
{
    private int id;
    private String name;
    private String type;
    private float price;
    private String description;
    private float weight;
    private int stock;

    public Product(int id, String name, String type, float price, String desc, float weight, int stock)
    {
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
        this.description = desc;
        this.weight = weight;
        this.stock = stock;
    }

    public Product(){}

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public float getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public float getWeight() {
        return weight;
    }

    public int getStock() {
        return stock;
    }

    @Override
    public String toString()
    {
        return name + "\nType: " + type + "\nPrice: " + price + " lei\nDescription: " + description + "\nWeight: " + weight + "kg\nStock: " + stock;
    }
}
