package Model;

public class Client
{
    private int id;
    private String fname;
    private String lname;
    private int age;
    private String email;
    private String address;

    public Client(int id, String fname, String lname, int age, String email, String address)
    {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.age = age;
        this.email = email;
        this.address = address;
    }

    public Client(){}

    public int getId() {
        return id;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public int getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString()
    {
        return "Contact name: " + fname + " " + lname + "\nAge: " + age + "\nEmail: " + email + "\nAddress: " + address;
    }
}
