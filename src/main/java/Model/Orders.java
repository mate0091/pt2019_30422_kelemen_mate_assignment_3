package Model;

import java.sql.Date;

public class Orders
{
    private int id;
    private int clientid;
    private int productid;
    private int amount;
    private float price_paid;
    private Date shipping_date;
    private String address;

    public Orders(int clientid, int productid, int amount, float price_paid, Date shipping_date, String address)
    {
        this.clientid = clientid;
        this.productid = productid;
        this.amount = amount;
        this.price_paid = price_paid;
        this.shipping_date = shipping_date;
        this.address = address;
    }

    public Orders(){}

    public int getClientid() {
        return clientid;
    }

    public int getProductid() {
        return productid;
    }

    public int getAmount() {
        return amount;
    }

    public float getPrice_paid() {
        return price_paid;
    }

    public Date getShipping_date() {
        return shipping_date;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "Amount: " + amount + "\nPrice Paid: " + price_paid + "lei\nShipping date: " + shipping_date.toString() + "\nDelivery Address: " + address;
    }
}
